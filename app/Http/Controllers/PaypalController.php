<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\PaymentDetails;
use Validator;

class PaypalController extends Controller
{
    protected $provider;

    public function payment(Request $request)
    {
        $data = [];
        $data['items'] = [
            [
                'name' => 'BeingAbundaceDonate',
                'price' => 0.01,
                'desc'  => 'Testing',
                'qty' => 1
            ]
        ];
  
        $data['invoice_id'] = 1;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('payment.success');
        $data['cancel_url'] = route('payment.cancel');
        $data['total'] = 0.01;
  
        $provider = new ExpressCheckout;
  
        $response = $provider->setExpressCheckout($data);
  
        $response = $provider->setExpressCheckout($data, true);
  
        return redirect($response['paypal_link']);
    }


    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        dd('Your payment is canceled. You can create cancel page here.');
    }


    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $data = [];

        $user_id  = Auth::User()->id;
        if(!empty($user_id)){

            $provider = new ExpressCheckout;

            $token = $request->get('token');
            
            $PayerID = $request->get('PayerID');
    
            $response = $provider->getExpressCheckoutDetails($token, $PayerID);

            // payment details 
            $data['payment_id']        = $PayerID;
            $data['item_id']           = 1;
            $data['email']             = $response['EMAIL'];
            $data['country_name']      = $response['COUNTRYCODE'];
            $data['currency_code']     = $response['CURRENCYCODE'];
            $data['fee_amount']        = $$response['L_TAXAMT0'];
            $data['taxamt']            = $response['L_AMT0'];
            $data['payment_date']      = $response['TIMESTAMP'];
            $data['payment_method']    = 'paypal';
            $data['payment_status']    = $response['ACK'];
            $data['user_id']           = Auth::User()->id;
            $data['message']           = 'Payment created successfully';
            PaymentDetails::create($data);

            if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                dd('Your payment was successfully. You can create success page here.');
            }

        } else {
            dd('Something is wrong.');
        }      

    }
}
