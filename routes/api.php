<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
 */

Route::post('register', 	'API\RegisterController@register');
Route::post('login', 		'API\RegisterController@login');

Route::post('addproduct', 	'API\RegisterController@addproduct');
Route::post('viewproduct', 	'API\RegisterController@viewproduct');

Route::post('update_carddetails', 	'API\RegisterController@update_carddetails');

Route::middleware('auth:api')->group( function () {
    Route::get('profile', 'API\RegisterController@viewprofile');
});


Route::group([
	'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});