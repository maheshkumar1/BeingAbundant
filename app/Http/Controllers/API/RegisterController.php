<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Validator;
use Hash;

use App\Mail\ForgotPassword;

use App\User;
use App\Product;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    
    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);
       
            if($validator->fails()){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['data'] = $validator->errors();
            }
    
            $input = $request->all();
            $user = User::where('email',$input['email']);
            // Email is already exists user
            if ($user->exists() > 0) {
                $data['status']   = false;
                $data['message']  = "Email already exists";
            } else {
                $input = $request->all();
                $input['password'] = Hash::make($input['password']);
                $user = User::create($input);
                $data['token'] =  $user->createToken('MyApp')->accessToken;

                $data['status'] = true;
                $data['status_code'] = '201';
                $data['message'] = 'User register successfully';
            }

          } catch (\Exception $e){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['message'] = $e->getMessage();
          }
    
          return response()->json($data, 200);
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
          try{
                $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
                ]);
        
                if ($validator->fails()) {          
                    $data['status'] = false;
                    $data['status_code'] = 404;
                    $data['data'] = $validator->errors();
                }  
                $request_data = $request->Input();

                $user = User::where('email' , $request_data['email']);

                if ($user->exists() > 0) {

                    $users = $user->get()->first();
                    if(Hash::check($request_data['password'], $users['password'])) {
                        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => '1'])){ 
                            $user = Auth::user(); 
                            $data['token'] =  $user->createToken('MyApp')-> accessToken; 
                            $data['first_name'] =  $user->first_name;
                            $data['last_name'] =  $user->last_name;
                            $data['message'] = 'User login successfully.';
                        } 
                        elseif(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => '0'])){
                            $data['status'] = false;
                            $data['status_code'] = 403;
                            $data['message'] = 'User denied';
                        }

                    } else {
                        $data['status']  = false;
                        $data['message'] = "Invalid Password";
                    }
                } else {
                    $data['status']  = false;
                    $data['message'] = "Invalid Email";
                }      
            } catch (\Exception $e){
                  $data['status'] = false;
                  $data['status_code'] = 404;
                  $data['message'] = $e->getMessage();
            }
            return response()->json($data, 200);
    }

    public function update_carddetails(Request $request){
        $result = [];
        try{
            $rules = array(
                'product_id'     => 'required',
                'user_id'        => 'required',
                'email'          => 'required|email',
            );

            $valid = Validator::make($request->all(), $rules);

            if ($valid->fails()) {

                $messages           = $valid->messages();
                $result['status']   = false;
                $result['messages'] = $messages;
                return response()->json($result);

            } else {
                $request_data = $request->Input();
                $users = User::where('email', $request_data['email']);
                if($users->count()>0){
                    $userdata = $users->get()->toArray();

                    
                    $current_card = json_decode($userdata[0]['card_open']); 
                    if(!is_array($current_card) ){
                        $current_card = array();
                    }
                    array_push($current_card, (int)$request_data['product_id']);
                    $users->update(['card_open' => $current_card]);
                    $result['status'] = true ;
                    $result['message'] ="success";

                } else {
                    $data['status']  = false;
                    $data['message'] = "Invalid Email";
                } 
            }

        } catch (\Exception $e){
                  $result['status'] = false;
                  $result['message'] = $e->getMessage();
        }
            return response()->json($result, 200);
    }

    public function viewprofile(Request $request)
    {
        try{
            $userProfile = User::where('id','=',Auth::user()->id)->get();

                if($userProfile->isEmpty()){
                    $data['status'] = false;
                    $data['status_code'] = 404;
                    $data['message'] = 'User profile details not found';  
                } else
                {
                    $data['status'] = true;
                    $data['status_code'] = 200;
                    $data['data'] = $userProfile; 
                    
                    $data['message'] = 'User profile details';
                }  

          } catch (\Exception $e){
                $data['status'] = false;
                $data['status_code'] = 404;
                $data['message'] = $e->getMessage();
          }
    
          return response()->json($data, 200);
    }
}
