<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    protected $table = 'payment_details';

    protected $fillable = [
        'payment_id', 'item_id', 'email', 'country_name', 'currency_code', 'fee_amount', 'taxamt', 'payment_date', 'payment_method', 'payment_status', 'user_id'
    ];
}